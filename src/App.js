import React, { useState, useCallback, useMemo, memo } from 'react';
import logo from './logo.svg';
import './App.css';

const defaultList = [
  { id: 1, name: "Mathieu", age: 23 },
  { id: 2, name: "Julien", age: 21 },
  { id: 3, name: "Alexandre", age: 30 },
  { id: 4, name: "Hélène", age: 50 },
  { id: 5, name: "Sébastien", age: 10 },
  { id: 6, name: "Eddy", age: 57 },
];


const List = memo(({ list, handleChangeName }) => {
  console.log("render")

  return <>{list.map((data) => {
    return <div key={data.id}>{data.name} {data.age} years <button onClick={(e) => handleChangeName(data.name)} >click to return name</button></div>
  })}</>

});

const Count = memo(({ title, count }) => {
  console.log(`render ${title}`)

  return <div style={{ marginBottom: "20px" }}>{title}={count}</div>

});



function computeList(list, count) {
  return list.map((data) => ({ ...data, age: data.age + count }))
}

export const App = () => {
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);
  const [nameState, setName] = useState("");

  const newList1Memo = useMemo(() => computeList(defaultList, count1), [count1]);
  const newList2Callback = useCallback(computeList(defaultList, count2), [count2]);

  const handleChangeName = useCallback((name) => {
    setName(name);
  }, [])

  return (
    <div className="App">
      <div style={{ marginBottom: "20px" }}>{nameState}</div>
      <div className="WrapperList">
        <div>
          <button onClick={(e) => setCount1(count1 + 1)}>Click to increase count 1</button>
          <Count title="Counter 1" count={count1} />
          <List list={newList1Memo} handleChangeName={handleChangeName} />
        </div>
        <div>
          <button onClick={(e) => setCount2(count2 + 1)}>Click to increase count 2</button>
          <Count title="Counter 2" count={count2} />
          <List list={newList2Callback} handleChangeName={handleChangeName} />
        </div>
      </div>
    </div>
  );
}
